#process.py

import csv
import requests

URL_CSV_FILE_SOURCE = "http://www.nber.org/data/chat/FinalCHAT_72909.csv"
LOCAL_CSV_FILE_SOURCE = "hist_adoption_tech.csv"

def retrieve():

    response = requests.get(URL_CSV_FILE_SOURCE)        
    with open(LOCAL_CSV_FILE_SOURCE, 'wb') as file:
        for chunk in response:
            file.write(chunk)
        file.close()

def run():
    retrieve()

if __name__ == '__main__':
    run()