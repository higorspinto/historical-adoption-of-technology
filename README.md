# Historical Adoption Of Technology

The Cross‐country Historical Adoption of Technology (CHAT) is an unbalanced panel dataset with information on the adoption of over 100 technologies in more than 150 countries since 1800. The dataset uses an annual frequency of observation. For some of the older technologies, like steamships, data go back until the early Nineteenth Century. The last year in the sample is 2003. Some data, especially in the earlier part of the sample, is not available at an annual frequency. 

## Data

The original dataset is available for download at: http://www.nber.org/data/chat.

The original paper is availabe at: https://www.nber.org/papers/w15319

The technology measures in CHAT capture a similar intuition. They are either: (i) the number of capital goods specifically related to accomplishing particular tasks, (ii) the amounts of particular tasks that have been accomplished, (iii) the number of users of a particular manner to accomplish a task. The definitions of all technologies in the dataset can be found in Table 1 of the original paper.

## Preparation

Data can be retrieved using the script `process.py`.
The script can be run as:

```
python process.py
```

Dependencies to run the script can be installed using the *requirements.txt* file.

```
pip install requirements.txt
```

## License

This Data Package is made available under the Public Domain Dedication and License v1.0 whose full text can be found at: [PPDL]

[PPDL]: http://www.opendatacommons.org/licenses/pddl/1.0/